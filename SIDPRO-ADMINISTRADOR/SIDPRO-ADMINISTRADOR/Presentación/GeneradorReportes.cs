﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SIDPRO_ADMINISTRADOR.Repositorio;

namespace SIDPRO_ADMINISTRADOR.Presentación
{
    public partial class GeneradorReportes : Form
    {
        private Sistema sistema;
        public GeneradorReportes()
        {
            InitializeComponent();
        }
        private void GeneradorReportes_Load(object sender, EventArgs e)
        {
            sistema = Sistema.Llamar;
            sistema.actualizar();
            lblEntregado.Text = sistema.cantEntregados.ToString();
            lblNoEntregado.Text = sistema.cantNoEntregados.ToString();

            for(int i=0; i<sistema.repEntregados.Count; i++)
            {
                dataGridView1.Rows.Add(sistema.repEntregados[i], sistema.infoEntregados[i]);
            }
            for(int i=0; i<sistema.repNoEntregados.Count; i++)
            {
                dataGridView2.Rows.Add(sistema.repNoEntregados[i], sistema.infoNoEntregados[i]);
            }
            dataGridView2.ForeColor = Color.Black;
        }

        private void tabControl1_DrawItem(object sender, DrawItemEventArgs e)
        {
            

            Graphics g = e.Graphics;
            Brush _textBrush;

            // Get the item from the collection.
            TabPage _tabPage = tabControl1.TabPages[e.Index];

            // Get the real bounds for the tab rectangle.
            Rectangle _tabBounds = tabControl1.GetTabRect(e.Index);

            if (e.State == DrawItemState.Selected)
            {

                // Draw a different background color, and don't paint a focus rectangle.
                _textBrush = new SolidBrush(Color.Black);
                g.FillRectangle(Brushes.Gray, e.Bounds);
            }
            else
            {
                _textBrush = new System.Drawing.SolidBrush(e.ForeColor);
                g.FillRectangle(Brushes.LightGray, e.Bounds);
            }

            // Use our own font.
            Font _tabFont = new Font("Arial", (float)12.0, FontStyle.Bold, GraphicsUnit.Pixel);

            // Draw string. Center the text.
            StringFormat _stringFlags = new StringFormat();
            _stringFlags.Alignment = StringAlignment.Center;
            _stringFlags.LineAlignment = StringAlignment.Center;
            g.DrawString(_tabPage.Text, _tabFont, _textBrush, _tabBounds, new StringFormat(_stringFlags));
        }

        private void button1_Click(object sender, EventArgs e)
        {
            sistema.actualizar();
            lblEntregado.Text = sistema.cantEntregados.ToString();
            lblNoEntregado.Text = sistema.cantNoEntregados.ToString();

            for (int i = 0; i < sistema.repEntregados.Count; i++)
            {
                dataGridView1.Rows.Add(sistema.repEntregados[i], sistema.infoEntregados[i]);
            }
            for (int i = 0; i < sistema.repNoEntregados.Count; i++)
            {
                dataGridView2.Rows.Add(sistema.repNoEntregados[i], sistema.infoNoEntregados[i]);
            }
        }
    }

       
}
