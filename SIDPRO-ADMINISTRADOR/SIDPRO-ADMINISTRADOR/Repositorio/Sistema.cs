﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SIDPRO_ADMINISTRADOR.Servicios;
using MySql.Data.MySqlClient;

namespace SIDPRO_ADMINISTRADOR.Repositorio
{
    class Sistema
    {
        private static Sistema instancia=null;
        public int cantEntregados;
        public int cantNoEntregados;
        public List<string> repEntregados;
        public List<string> repNoEntregados;
        public List<string> infoEntregados;
        public List<string> infoNoEntregados;
        private Sistema()
        {

        }
        internal static Sistema Llamar
        {
            get
            {
                if (instancia == null)
                    instancia = new Sistema();
                return instancia;
            }
        }

        private void actualizarEntregados()
        {
            cantEntregados = 0;
            repEntregados = new List<string>();
            infoEntregados = new List<string>();

            MySqlConnection miCon = ConexionBD.Instancia;
            miCon.Open();
            MySqlCommand cmd = miCon.CreateCommand();
            cmd.CommandText = "SELECT * FROM sidpro.reportes_entregado;";
            MySqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                repEntregados.Add(reader.GetString("codigoReporte"));
                infoEntregados.Add(reader.GetString("Informacion"));
                cantEntregados++;
            }
            miCon.Close();
        }

        private void actualizarNoEntregados()
        {
            cantNoEntregados = 0;
            repNoEntregados = new List<string>();
            infoNoEntregados = new List<string>();

            MySqlConnection miCon = ConexionBD.Instancia;
            miCon.Open();
            MySqlCommand cmd = miCon.CreateCommand();
            cmd.CommandText = "SELECT * FROM sidpro.reportes_no_entregado;";
            MySqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                repNoEntregados.Add(reader.GetString("codigoReporte"));
                infoNoEntregados.Add(reader.GetString("Informacion"));
                cantNoEntregados++;
            }
            miCon.Close();
        }

        public void actualizar()
        {
            actualizarEntregados();
            actualizarNoEntregados();
        }
    }
}
