﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using SIDPRO_SISTEMAINTELIGENTE.Servicios;
using SIDPRO_SISTEMAINTELIGENTE.Presentación;
using System.Windows.Forms;

namespace SIDPRO_SISTEMAINTELIGENTE.Repositorios
{
    class SI
    {

        private static SI unico = null; //Patrón singleton para tener una sola instancia
        private MySqlConnection miCon = null; // Conexion base de datos
        private double umbral = 0.038521686070092; // Distancia máxima entre dos puntos para pertenecer a una zona
        private double distanciaMaxima = 0.0571071; // Distancia máxima para recorrer en bicicleta
        private float volumenMax = 27000f; // Volumen máximo para una motocicleta y bicicleta
        private double latAlmacen = -12.053329, lngAlmacen = -77.085559;
        private List<Paquete> Paquetes=null; //Lista de paquetes completa
        private List<List<Paquete>> paquetesDivididos = null; //Lista de paquetes divididos
        private List<bool> Seleccionado = null; //Lista de booleanos asociados a los paquetes para la selección de las zonas
        private List<Zona> zonas = null; //Lista de objetos Zona, que contiene el polígono de la zona

        private string sentenciaNombres = "";
        private string sentenciaVehicul = "";
        private int cantBici = 0;
        private int cantMoto = 0;
        private int cantMini = 0;

        private SI()
        {
        }

        private bool verificarEscaneo()
        {
            miCon = ConexionBD.Instancia;// Se crea una base de datos
            miCon.Open();// Se abre la base de datos
            // Se crea un comando de selección
            MySqlCommand cmd = miCon.CreateCommand();
            cmd.CommandText = "SELECT * FROM sidpro.sistema;";
            //Se crea un DataReader para almacenar la tabla y se ejecuta el comando
            MySqlDataReader reader = cmd.ExecuteReader();
            bool escaneado=false;
            //Se empieza a leer la tabla guardad en el DataREader
            while (reader.Read())
            {
                escaneado = reader.GetBoolean("escaneado");
            }
            //Se cierra la base de datos
            miCon.Close();
            return escaneado;
        }

        private void verificarHorarios()
        {
            miCon = ConexionBD.Instancia;// Se crea una base de datos
            miCon.Open();// Se abre la base de datos
            // Se crea un comando de selección
            MySqlCommand cmd = miCon.CreateCommand();
            cmd.CommandText = "SELECT codigo,Nombres,Apellidos,Teléfono,Correo,Horario FROM sidpro.paquete";
            //Se crea un DataReader para almacenar la tabla y se ejecuta el comando
            MySqlDataReader reader = cmd.ExecuteReader();
            short horario;
            string nombres, apellidos, correo, telefono, codigo;
            //Se empieza a leer la tabla guardad en el DataREader
            while (reader.Read())
            {
                codigo = reader.GetString("codigo");
                nombres = reader.GetString("Nombres");
                apellidos = reader.GetString("Apellidos");
                telefono = reader.GetString("Teléfono");
                correo = reader.GetString("Correo");
                try
                {
                    //En el caso de que retorne NULL (Horario no escogido)
                    horario = reader.GetInt16("Horario");
                }
                catch (System.Data.SqlTypes.SqlNullValueException)
                {
                    //Se genera una instancia de seleccionar horario
                    Application.Run(new seleccionHorario(codigo, nombres, apellidos, telefono, correo));
                }
            }
            //Se cierra la base de datos
            miCon.Close();
        }
        private void obtenerPaquetes()
        {
            //Se instancia el atributo PAquete
            Paquetes = new List<Paquete>();
            miCon = ConexionBD.Instancia; //Se crea la conexión a la base de datos
            miCon.Open(); // Se aber la conexión a la base de datos
            //Se crea un comando de selección
            MySqlCommand cmd = miCon.CreateCommand();
            cmd.CommandText = "SELECT * FROM sidpro.paquete";
            //Se crea un DataReader que almacenará los datos de los paquetes para su instanciación
            MySqlDataReader reader = cmd.ExecuteReader();
            //Se lee la tabla y se agregan paquetes a la lista
            while (reader.Read())
            {
                medidas medida = new medidas(reader.GetFloat("Ancho"), reader.GetFloat("Largo"), reader.GetFloat("Alto"));
                coord coordenada = new coord(reader.GetDouble("Lat"), reader.GetDouble("Long"));
                Paquetes.Add(new Paquete(reader.GetString("codigo"), coordenada, reader.GetBoolean("Fragil"), medida, reader.GetInt16("Horario")));
            }
            //Se cierra la base de datos
            miCon.Close();
        }

        private void dividirPorZonas()
        {
            //Se instancian los atributos
            Seleccionado = new List<bool>();
            paquetesDivididos = new List<List<Paquete>>();
            zonas = new List<Zona>();
            // Se inicializa todos los booleanos con falso
            for (int a = 0; a < Paquetes.Count; a++)
            {
                Seleccionado.Add(false);
            }
            int i = 0;
            // Primera zona nombrada Zona A
            char nZona = 'A';

            // Se selecciona un paquete central y se evalua la proximidad con el resto de paquetes
            foreach (Paquete pInicial in Paquetes)
            {
                string sentencia = "";
                //Si el paquete aún no ha sido seleccionado para pertenecer a una zona, se evalua
                if (!Seleccionado[i])
                {

                    List<Paquete> division = new List<Paquete>();

                    double latmax, latmin, lngmax, lngmin;

                    division.Add(pInicial);
                    Seleccionado[i] = true;

                    // Se actualiza la base de datos
                    sentencia = sentencia + "\nUPDATE `sidpro`.`paquete` SET `codigoZona`='"+nZona.ToString()+"' WHERE `codigo`='"+pInicial.Codigo+"';";

                    // Se inicializan los extremos
                    latmax = pInicial.Coordenadas.lat; latmin = pInicial.Coordenadas.lat; lngmax = pInicial.Coordenadas.lng; lngmin = pInicial.Coordenadas.lng;
                    int j = 0;
                    foreach (Paquete p in Paquetes)
                    {
                        //Si el paquete no fue seleccionado, se evalua la proximidad con el primer paquete
                        if (!Seleccionado[j])
                        {
                            //Se calcula la distancia entre los dos puntos
                            double distancia = Math.Sqrt(Math.Pow(pInicial.Coordenadas.lat - p.Coordenadas.lat, 2) + Math.Pow(pInicial.Coordenadas.lng - p.Coordenadas.lng, 2));
                            //Si la distancia es menor que el umbral se almacena
                            if (distancia <= umbral)
                            {
                                division.Add(p);
                                Seleccionado[j] = true;

                                //Se actualiza la BD

                                sentencia = sentencia + "\nUPDATE `sidpro`.`paquete` SET `codigoZona`='" + nZona.ToString() + "' WHERE `codigo`='" + p.Codigo + "';\n";
                                //Se evaluan los extremos
                                if (p.Coordenadas.lat < latmin)
                                    latmin = p.Coordenadas.lat;
                                if (p.Coordenadas.lat > latmax)
                                    latmax = p.Coordenadas.lat;
                                if (p.Coordenadas.lng < lngmin)
                                    lngmin = p.Coordenadas.lng;
                                if (p.Coordenadas.lng > lngmax)
                                    lngmax = p.Coordenadas.lng;
                            }
                        }
                        j++;
                    }
                    //Se agrega la nueva division (Con paquetes) a la lista de paquetes divididos
                    paquetesDivididos.Add(division);

                    //Se agrega una nueva zona a la lista de zonas
                    zonas.Add(new Zona(nZona.ToString(),latmax,latmin,lngmax,lngmin));

                    //Se almacena la zona en la BD

                    sentenciaNombres = sentenciaNombres+"INSERT INTO `sidpro`.`zona` (`codigoZona`, `cantidadPaquetes`, `vehiculo`, `latMax`, `latMin`, `lngMax`, `lngMin`) VALUES('" + nZona.ToString() + "', '" + division.Count + "', NULL, '" + latmax + "', '" + latmin + "', '" + lngmax + "', '" + lngmin + "');" + sentencia;

                    //
                    nZona++;
                }
                i++;
            }
        }

        private void seleccionarVehiculos()
        {
            float volTot = 0;
            short vehiculo;
            int cantPaquetes,i=0;
            double latMed = 0, lngMed = 0;
            foreach(List<Paquete> miLista in paquetesDivididos)
            {
                volTot = 0;
                latMed = 0; lngMed = 0;
                cantPaquetes = miLista.Count;
                foreach(Paquete p in miLista)
                {
                    volTot += (p.Medida.alto * p.Medida.ancho * p.Medida.largo);
                    latMed += p.Coordenadas.lat;
                    lngMed += p.Coordenadas.lng;
                }
                latMed /= cantPaquetes;
                lngMed /= cantPaquetes;
                if(volTot < volumenMax)
                {
                    double d = Math.Sqrt(Math.Pow(latAlmacen - latMed, 2) + Math.Pow(lngAlmacen - lngMed, 2));
                    if (d < distanciaMaxima)
                    {
                        vehiculo = 1; // Bicicleta
                        cantBici++;
                        
                    }else
                    {
                        vehiculo = 2; // Motocicleta
                        cantMoto++;
                    }
                }
                else
                {
                    vehiculo = 3; // Minivan
                    cantMini++;
                }
                zonas[i].Vehiculo = vehiculo;
                sentenciaVehicul = sentenciaVehicul + "UPDATE `sidpro`.`zona` SET `vehiculo`='" + vehiculo + "' WHERE `codigoZona`='" + zonas[i].Nombre + "';\n";
                i++;
            }
        }

        private void almacenarEnLaBD()
        {
            miCon = ConexionBD.Instancia; // Se crea una conexión
            miCon.Open(); // Se abre la conexión
                          // Se crea un comando para ejecutar en MySQL
            MySqlCommand cmd = miCon.CreateCommand();
            cmd.CommandText = sentenciaNombres + sentenciaVehicul + "UPDATE `sidpro`.`sistema` SET `escaneado`='1' WHERE `escaneado`='0';";
            // Se ejecuta el comando
            cmd.ExecuteNonQuery();
            // Se cierra la conexión y la ventana
            miCon.Close();
        }

        //Llamada a la única instancia de Sistema Inteligente(SI) mediante el patrón singleton
        public static SI Llamar
        {
            get
            {
                
                if (unico == null)
                    unico = new SI();                
                return unico;
            }
        }

        public int CantBici
        {
            get
            {
                return cantBici;
            }
        }

        public int CantMoto
        {
            get
            {
                return cantMoto;
            }

        
        }

        public int CantMini
        {
            get
            {
                return cantMini;
            }
        }

        internal List<Zona> Zonas
        {
            get
            {
                return zonas;
            }

            set
            {
                zonas = value;
            }
        }

        internal List<Paquete> Paquetes1
        {
            get
            {
                return Paquetes;
            }

            set
            {
                Paquetes = value;
            }
        }

        public double LatAlmacen
        {
            get
            {
                return latAlmacen;
            }

            set
            {
                latAlmacen = value;
            }
        }

        public double LngAlmacen
        {
            get
            {
                return lngAlmacen;
            }

            set
            {
                lngAlmacen = value;
            }
        }

        public void iniciar()
        {
            if (!verificarEscaneo()) //Para volver a verificar las zonas hará falta cambiar el valor a NULL a cada celda de la columna tabla.paquetes
            {                        //Eliminar todos los datos de la tabla sidpro.zona para crear nuevamente las zonas
                verificarHorarios(); //Y cambiar el valor de la columna 'escaneado' de la tabla sidpro.sistema
                obtenerPaquetes();   //Ya que no tuvimos tiempo de implementar la actualización de la BD
                dividirPorZonas();
                seleccionarVehiculos();
                almacenarEnLaBD();
                CREACION_DE_ZONAS cz = new CREACION_DE_ZONAS();
                Application.Run(cz);
                DISTRIBUCION_TRANSPORTE dt = new DISTRIBUCION_TRANSPORTE();
                Application.Run(dt);
            }else
            {
                MessageBox.Show("Se ha escaneado recientemente");
            }
        }
    }
}
