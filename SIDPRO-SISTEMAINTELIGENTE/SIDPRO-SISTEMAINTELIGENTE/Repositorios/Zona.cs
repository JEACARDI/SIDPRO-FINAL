﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GMap.NET.WindowsForms;
using GMap.NET;
using GMap.NET.MapProviders;

namespace SIDPRO_SISTEMAINTELIGENTE.Repositorios
{
    class Zona
    {
        private GMapPolygon poligono = null;
        private double separacion = 0.001;
        private double latMax, latMin, lngMax, lngMin;
        private double latMed = 0, lngMed = 0;
        private string nombre;
        private short vehiculo;

        public Zona(string nombre,double latMax,double latMin,double lngMax,double lngMin)
       {
            this.nombre = nombre;
            this.latMax = latMax;
            this.latMin = latMin;
            this.lngMax = lngMax;
            this.lngMin = lngMin;
        }

        public GMapPolygon Poligono
        {
            get
            {
                //Se genera un cuadrante teniendo en cuenta las coordenadas máximas y mínimas
                List<PointLatLng> puntos = new List<PointLatLng>();
                puntos.Add(new PointLatLng(latMin - separacion, lngMin - separacion));
                puntos.Add(new PointLatLng(latMin - separacion, lngMax + separacion));
                puntos.Add(new PointLatLng(latMax + separacion, lngMax + separacion));
                puntos.Add(new PointLatLng(latMax + separacion, lngMin - separacion));
                poligono = new GMapPolygon(puntos, nombre);
                return poligono;
            }

            set
            {
                poligono = value;
            }
        }
        public string Nombre
        {
            get
            {
                return nombre;
            }

            set
            {
                nombre = value;
            }
        }

        public short Vehiculo
        {
            get
            {
                return vehiculo;
            }

            set
            {
                vehiculo = value;
            }
        }

        public double LatMax
        {
            get
            {
                return latMax;
            }

            set
            {
                latMax = value;
            }
        }
    }
}
