﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIDPRO_SISTEMAINTELIGENTE.Repositorios
{
    struct coord
    {
        public double lat;
        public double lng;
        public coord(double lat, double lng)
        {
            this.lat = lat; this.lng = lng;
        }
    }
    struct medidas
    {
        public float ancho;
        public float largo;
        public float alto;
        public medidas(float ancho, float largo, float alto)
        {
            this.ancho = ancho; this.largo = largo; this.alto = alto;
        }
    }
    class Paquete
    {
        private string codigo;
        private coord coordenadas;
        private bool fragil;
        private medidas medida;
        private string idRepartidor;
        private short horario;

        public Paquete(string codigo, coord coordenadas, bool fragil, medidas medida, short horario)
        {
            this.codigo = codigo;
            this.fragil = fragil;
            this.horario = horario;
            this.coordenadas = coordenadas;
            this.medida = medida;
        }

        public string Codigo
        {
            get
            {
                return codigo;
            }

            set
            {
                codigo = value;
            }
        }

        public coord Coordenadas
        {
            get
            {
                return coordenadas;
            }

            set
            {
                coordenadas = value;
            }
        }

        public bool Fragil
        {
            get
            {
                return fragil;
            }

            set
            {
                fragil = value;
            }
        }

        public medidas Medida
        {
            get
            {
                return medida;
            }

            set
            {
                medida = value;
            }
        }

        public string IdRepartidor
        {
            get
            {
                return idRepartidor;
            }

            set
            {
                idRepartidor = value;
            }
        }

        public short Horario
        {
            get
            {
                return horario;
            }

            set
            {
                horario = value;
            }
        }
    }

}
