﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using SIDPRO_SISTEMAINTELIGENTE.Repositorios;

namespace SIDPRO_SISTEMAINTELIGENTE
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            /*Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());*/
            SI sistema = SI.Llamar;
            sistema.iniciar();
        }
    }
}
