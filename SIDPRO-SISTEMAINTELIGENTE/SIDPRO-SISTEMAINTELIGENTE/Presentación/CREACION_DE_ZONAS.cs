﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GMap.NET.WindowsForms;
using GMap.NET;
using GMap.NET.MapProviders;
using GMap.NET.WindowsForms.Markers;
using GMap.NET.WindowsForms.ToolTips;
using SIDPRO_SISTEMAINTELIGENTE.Repositorios;

namespace SIDPRO_SISTEMAINTELIGENTE.Presentación
{
    public partial class CREACION_DE_ZONAS : Form
    {
        private static SI sistema;
        
        private GMapOverlay poligonoOverlay;
        private GMapOverlay almacenOverlay;
        private GMapMarker almacenMarker;
        private List<Zona> misZonas;
        private List<Paquete> misPaquetes;
        public CREACION_DE_ZONAS()
        {
            sistema = SI.Llamar;
            poligonoOverlay = new GMapOverlay("Poligono2");
            almacenOverlay = new GMapOverlay("Marcador2");
            
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterScreen;
        }

        private void CREACION_DE_ZONAS_Load(object sender, EventArgs e)
        {
            misZonas = sistema.Zonas;
            misPaquetes = sistema.Paquetes1;
            //Iniciar el mapa
            gMapControl1.DragButton = MouseButtons.Left;
            gMapControl1.CanDragMap = true;
            gMapControl1.MapProvider = GMapProviders.GoogleMap;
            gMapControl1.Position = new PointLatLng(sistema.LatAlmacen, sistema.LngAlmacen);
            gMapControl1.MinZoom = 0;
            gMapControl1.MaxZoom = 24;
            gMapControl1.Zoom = 12;
            gMapControl1.AutoScroll = true;

            gMapControl1.Overlays.Add(poligonoOverlay);
            gMapControl1.Overlays.Add(almacenOverlay);


            for (int i = 0; i < misZonas.Count; i++)
            {
                poligonoOverlay.Polygons.Add(misZonas[i].Poligono);
            }

            for (int i = 0; i < misPaquetes.Count; i++)
            {
                almacenOverlay.Markers.Add(new GMarkerGoogle(new PointLatLng(misPaquetes[i].Coordenadas.lat, misPaquetes[i].Coordenadas.lng), GMarkerGoogleType.green));
            }
            almacenOverlay.Markers.Add(new GMarkerGoogle(new PointLatLng(sistema.LatAlmacen, sistema.LngAlmacen), GMarkerGoogleType.red));
            gMapControl1.Zoom += 1;
            gMapControl1.Zoom -= 1;
        }
    }
}
