﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using SIDPRO_SISTEMAINTELIGENTE.Repositorios;

namespace SIDPRO_SISTEMAINTELIGENTE.Presentación
{
    public partial class DISTRIBUCION_TRANSPORTE : Form
    {
        private static MySqlConnection conex = null;
        private SI sist = null;
        public DISTRIBUCION_TRANSPORTE()
        {
            InitializeComponent();
        }

        private void DISTRIBUCION_TRANSPORTE_Load(object sender, EventArgs e)
        {
            sist = SI.Llamar;
            lblcantBicicletas.Text =Convert.ToString(sist.CantBici);
            lblcantMoto.Text = Convert.ToString(sist.CantMoto);
            lblcantCarros.Text = Convert.ToString(sist.CantMini);
            cantidadZonas.Text = Convert.ToString(sist.CantBici+sist.CantMoto+sist.CantMini);

        }
    }
}
