﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using SIDPRO_SISTEMAINTELIGENTE.Servicios;

namespace SIDPRO_SISTEMAINTELIGENTE.Presentación
{
    public partial class seleccionHorario : Form
    {
        MySqlConnection miCon;
        private string codigo;
        public seleccionHorario(string codigo, string nombres, string apellidos, string telefono, string correo)
        {
            InitializeComponent();
            lblNombres.Text = nombres;
            lblApellidos.Text = apellidos;
            lblTelefono.Text = telefono;
            lblCorreo.Text = correo;
            cmbHorario.SelectedIndex = 0;
            this.codigo = codigo;
        }

        private void seleccionHorario_Load(object sender, EventArgs e)
        {
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {

            if(cmbHorario.SelectedIndex != 0) // Si el horario elegido es incorrecto
            {
                miCon = ConexionBD.Instancia; // Se crea una conexión
                miCon.Open(); // Se abre la conexión
                // Se crea un comando para ejecutar en MySQL
                MySqlCommand cmd = miCon.CreateCommand();
                cmd.CommandText = "UPDATE sidpro.paquete SET Horario=" + cmbHorario.SelectedIndex + " WHERE codigo='" + codigo + "';";
                // Se ejecuta el comando
                cmd.ExecuteNonQuery();
                // Se cierra la conexión y la ventana
                miCon.Close();
                MessageBox.Show("Horario de entrega seleccionado!");
                Dispose();
            }
            else
            {
                MessageBox.Show("Debe elegir un horario de entrega!");
            }
        }
    }
}
