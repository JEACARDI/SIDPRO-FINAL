﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GMap.NET.WindowsForms;
using GMap.NET;
using GMap.NET.MapProviders;
using GMap.NET.WindowsForms.Markers;
using GMap.NET.WindowsForms.ToolTips;
using SIDPRO_REPARTIDOR.Repositorios;
using MySql.Data.MySqlClient;
using SIDPRO_REPARTIDOR.Servicios;

namespace SIDPRO_REPARTIDOR.Presentación
{
    public partial class SelectorZonas : Form
    {
        private GMapOverlay poligonoOverlay;
        private GMapOverlay almacenOverlay;
        private string idRepartidor;
        private GMapMarker almacenMarker;
        private DataTable tablaZonas;
        private MySqlConnection miCon;
        private List<Zona> zonas;
        public SelectorZonas(string idRepartidor)
        {
            poligonoOverlay = new GMapOverlay("Poligono1");
            almacenOverlay = new GMapOverlay("Marcador1");
            this.idRepartidor = idRepartidor;
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterScreen;
        }

        private void SelectorZonas_Load(object sender, EventArgs e)
        {
            //Iniciar el mapa
            gMapControl1.DragButton = MouseButtons.Left;
            gMapControl1.CanDragMap = true;
            gMapControl1.MapProvider = GMapProviders.GoogleMap;
            gMapControl1.MinZoom = 0;
            gMapControl1.MaxZoom = 24;
            gMapControl1.AutoScroll = true;

            gMapControl1.Overlays.Add(poligonoOverlay);
            gMapControl1.Overlays.Add(almacenOverlay);

            refrescar();
        }

        private void MostrarZona(object sender, DataGridViewCellMouseEventArgs e)
        {
            int fila = e.RowIndex;
            try
            {
                GMapPolygon poligono = zonas[fila].Poligono;
                poligonoOverlay.Clear();
                poligonoOverlay.Polygons.Add(poligono);
                gMapControl1.Position = new PointLatLng(zonas[fila].LatMed, zonas[fila].LngMed);
                //actualizar el mapa
                actualizarMapa();

                label4.Text = Convert.ToString(zonas[fila].CantidadPaquetes);

                if (zonas[fila].Vehiculo == 1)
                    label6.Text = "Bicicleta";
                else if (zonas[fila].Vehiculo == 2)
                    label6.Text = "Motocicleta";
                else if (zonas[fila].Vehiculo == 3)
                    label6.Text = "Minivan";
                else
                    label6.Text = "Desonocido";

                if (zonas[fila].IdRepartidor == null)
                {
                    label8.Text = "Ninguno";
                }
                else
                {
                    label8.Text = zonas[fila].IdRepartidor;
                }
            }
            catch(ArgumentOutOfRangeException)
            {
            }
        }

        private void actualizarMapa()
        {
            //Se agrega el marcador del almacen
            almacenOverlay.Clear();
            almacenMarker = new GMarkerGoogle(new PointLatLng(Sistema.LatAlmacen, Sistema.LngAlmacen), GMarkerGoogleType.red);
            almacenOverlay.Markers.Add(almacenMarker);

            //actualizar el mapa
            gMapControl1.Zoom = gMapControl1.Zoom + 1;
            gMapControl1.Zoom = gMapControl1.Zoom - 1;
        }

        private void AgregarZona(object sender, DataGridViewCellEventArgs e)
        {
            int fila = e.RowIndex;
            try
            {
                if(zonas[fila].IdRepartidor == null)
                {
                    zonas[fila].IdRepartidor = idRepartidor;
                    MessageBox.Show("Zona elegida!");
                    label8.Text = idRepartidor;

                    //Se actualiza la bD
                    miCon = ConexionBD.Instancia;
                    miCon.Open(); // Se abre la conexión
                    // Se crea un comando para ejecutar en MySQL
                    MySqlCommand cmd = miCon.CreateCommand();
                    cmd.CommandText = "UPDATE `sidpro`.`zona` SET `codigoRepartidor`='"+idRepartidor+"' WHERE `codigoZona`='"+ zonas[fila].Nombre+"';";
                    // Se ejecuta el comando
                    cmd.ExecuteNonQuery();
                    // Se cierra la conexión y la ventana
                    miCon.Close();
                    //
                }
                else
                {
                    if(zonas[fila].IdRepartidor == idRepartidor)
                    {
                        zonas[fila].IdRepartidor = null;
                        MessageBox.Show("Esta zona ya no le pertenece!");
                        label8.Text = "Ninguno";

                        string nombre = zonas[fila].Nombre;

                        //Se actualiza la bD
                        miCon = ConexionBD.Instancia;
                        miCon.Open(); // Se abre la conexión
                        // Se crea un comando para ejecutar en MySQL
                        MySqlCommand cmd = miCon.CreateCommand();
                        cmd.CommandText = "UPDATE `sidpro`.`zona` SET `codigoRepartidor`=NULL WHERE `codigoZona`='"+ zonas[fila].Nombre+"';";
                        // Se ejecuta el comando
                        cmd.ExecuteNonQuery();
                        // Se cierra la conexión y la ventana
                        miCon.Close();
                    }
                    else
                        MessageBox.Show("Esta zona ya fue elegida por alguien más!");
                }
            }
            catch (ArgumentOutOfRangeException)
            {
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //sistema.MisZonas = misZonas;
            LISTA_DE_ZONAS ls = new LISTA_DE_ZONAS(idRepartidor);
            ls.Show();
            this.Visible = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            refrescar();
        }

        private void refrescar()
        {
            zonas = Sistema.obtenerZonas();
            //Refrescar el mapa
            if(zonas.Count!=0)
                gMapControl1.Position = new PointLatLng(zonas[0].LatMed, zonas[0].LngMed);
            gMapControl1.Zoom = 12;

            //Agregar el primer polígono
            GMapPolygon poligono = null;
            if (zonas.Count != 0)
            {
                poligono = zonas[0].Poligono;
                poligonoOverlay.Clear();
                poligonoOverlay.Polygons.Add(poligono);
            }
            //
            actualizarMapa();

            tablaZonas = new DataTable();
            tablaZonas.Columns.Add(new DataColumn("Zonas", typeof(string)));
            char inicio = 'A';

            for (int i = 0; i < zonas.Count; i++)
            {
                tablaZonas.Rows.Add("Zona " + Convert.ToString(inicio));
                inicio++;
            }

            dataGridView1.DataSource = tablaZonas;
            if (zonas.Count != 0)
            {
                dataGridView1.Columns[0].Width = 210;
                dataGridView1.Columns[0].Resizable = DataGridViewTriState.False;
                dataGridView1.Columns[0].ReadOnly = true;

                label4.Text = Convert.ToString(zonas[0].CantidadPaquetes);
                if (zonas[0].Vehiculo == 1)
                    label6.Text = "Bicicleta";
                else if (zonas[0].Vehiculo == 2)
                    label6.Text = "Motocicleta";
                else if (zonas[0].Vehiculo == 3)
                    label6.Text = "Minivan";
                else
                    label6.Text = "Desonocido";

                if (zonas[0].IdRepartidor == null)
                {
                    label8.Text = "Ninguno";
                }
                else
                {
                    label8.Text = zonas[0].IdRepartidor;
                }
                actualizarMapa();
            }
        }
    }
}
