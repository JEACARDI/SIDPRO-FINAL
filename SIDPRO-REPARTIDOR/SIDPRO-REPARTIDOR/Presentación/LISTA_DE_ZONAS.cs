﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GMap.NET.WindowsForms;
using GMap.NET;
using GMap.NET.MapProviders;
using GMap.NET.WindowsForms.Markers;
using GMap.NET.WindowsForms.ToolTips;
using SIDPRO_REPARTIDOR.Repositorios;
using MySql.Data.MySqlClient;
using SIDPRO_REPARTIDOR.Servicios;

namespace SIDPRO_REPARTIDOR.Presentación
{
    public partial class LISTA_DE_ZONAS : Form
    {

        private LISTA_DE_PAQUETES lp;
        private string idRepartidor;
        private GMapOverlay poligonoOverlay;
        private GMapOverlay almacenOverlay;
        private GMapMarker almacenMarker;
        private List<Zona> misZonas;

        public LISTA_DE_ZONAS(string idRepartidor)
        {
            poligonoOverlay = new GMapOverlay("Poligono2");
            almacenOverlay = new GMapOverlay("Marcador2");
            this.idRepartidor = idRepartidor;

            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterScreen;

        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
          int fila = e.RowIndex;
            try
             {
                if (misZonas != null)
                {
                    lp = new LISTA_DE_PAQUETES(misZonas[fila].Nombre);
                    lp.Show();
                }
            }
            catch (ArgumentOutOfRangeException)
            {
            }
        }

        private void actualizarMapa()
        {
            //Se agrega el marcador del almacen
            almacenOverlay.Clear();
            almacenMarker = new GMarkerGoogle(new PointLatLng(Sistema.LatAlmacen, Sistema.LngAlmacen), GMarkerGoogleType.red);
            almacenOverlay.Markers.Add(almacenMarker);

            //actualizar el mapa
            gMapControl1.Zoom = gMapControl1.Zoom + 1;
            gMapControl1.Zoom = gMapControl1.Zoom - 1;
        }

        private void dataGridView1_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            int fila = e.RowIndex;
            try
            {
                if (misZonas != null)
                {
                    GMapPolygon poligono = misZonas[fila].Poligono;
                    poligonoOverlay.Clear();
                    poligonoOverlay.Polygons.Add(poligono);
                    gMapControl1.Position = new PointLatLng(misZonas[fila].LatMed, misZonas[fila].LngMed);
                    //actualizar el mapa
                    actualizarMapa();
                }
            }
            catch (ArgumentOutOfRangeException)
            {
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            refrescar();
        }

        private void refrescar()
        {
            misZonas = Sistema.obtenerMisZonas(idRepartidor);
            //Iniciar el mapa
            if (misZonas != null)
                gMapControl1.Position = new PointLatLng(misZonas[0].LatMed, misZonas[0].LngMed);
            gMapControl1.Zoom = 12;

            if (misZonas != null)
            {
                dataGridView1.Rows.Clear();
                for (int i = 0; i < misZonas.Count; i++)
                {
                    string vehiculo;
                    if (misZonas[i].Vehiculo == 1)
                        vehiculo = "Bicicleta";
                    else if (misZonas[i].Vehiculo == 2)
                        vehiculo = "Motocicleta";
                    else if (misZonas[i].Vehiculo == 3)
                        vehiculo = "Minivan";
                    else
                        vehiculo = "Desconocido";
                    dataGridView1.Rows.Add(misZonas[i].Nombre, "Cantidad de paquetes: " + misZonas[i].CantidadPaquetes + " Vehículo a usar: " + vehiculo);
                }

                poligonoOverlay.Clear();
                poligonoOverlay.Polygons.Add(misZonas[0].Poligono);
            }
            actualizarMapa();
        }


        private void LISTA_DE_ZONAS_Load(object sender, EventArgs e)
        {
            //Iniciar el mapa
            gMapControl1.DragButton = MouseButtons.Left;
            gMapControl1.CanDragMap = true;
            gMapControl1.MapProvider = GMapProviders.GoogleMap;
            gMapControl1.MinZoom = 0;
            gMapControl1.MaxZoom = 24;
            gMapControl1.AutoScroll = true;

            gMapControl1.Overlays.Add(poligonoOverlay);
            gMapControl1.Overlays.Add(almacenOverlay);

            label2.Text = label2.Text + idRepartidor;
            refrescar();
        }
    }
}
