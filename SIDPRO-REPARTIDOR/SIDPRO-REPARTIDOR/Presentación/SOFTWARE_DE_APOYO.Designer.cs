﻿namespace SIDPRO_REPARTIDOR.Presentación
{
    partial class SOFTWARE_DE_APOYO
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.gMapControl1 = new GMap.NET.WindowsForms.GMapControl();
            this.tpInformacionPaq = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.linkLblFragil = new System.Windows.Forms.LinkLabel();
            this.lbltxHORA = new System.Windows.Forms.Label();
            this.lbltxESTADO = new System.Windows.Forms.Label();
            this.labeltxCLiente = new System.Windows.Forms.Label();
            this.lblEstado = new System.Windows.Forms.Label();
            this.lblHora = new System.Windows.Forms.Label();
            this.lblCliente = new System.Windows.Forms.Label();
            this.tpGenerarRep = new System.Windows.Forms.TabPage();
            this.rbNoEntregado = new System.Windows.Forms.RadioButton();
            this.rbEntregado = new System.Windows.Forms.RadioButton();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblDnii = new System.Windows.Forms.Label();
            this.lblCLIENTE2 = new System.Windows.Forms.Label();
            this.lblDNI = new System.Windows.Forms.Label();
            this.lblCiente2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSiguiente = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tpInformacionPaq.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tpGenerarRep.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Alignment = System.Windows.Forms.TabAlignment.Left;
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tpInformacionPaq);
            this.tabControl1.Controls.Add(this.tpGenerarRep);
            this.tabControl1.DrawMode = System.Windows.Forms.TabDrawMode.OwnerDrawFixed;
            this.tabControl1.ItemSize = new System.Drawing.Size(152, 150);
            this.tabControl1.Location = new System.Drawing.Point(26, 46);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.Padding = new System.Drawing.Point(30, 70);
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(886, 462);
            this.tabControl1.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabControl1.TabIndex = 3;
            this.tabControl1.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.tabControl1_DrawItem);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.gMapControl1);
            this.tabPage1.Location = new System.Drawing.Point(154, 4);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(728, 454);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "MAPA";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // gMapControl1
            // 
            this.gMapControl1.Bearing = 0F;
            this.gMapControl1.CanDragMap = true;
            this.gMapControl1.EmptyTileColor = System.Drawing.Color.Navy;
            this.gMapControl1.GrayScaleMode = false;
            this.gMapControl1.HelperLineOption = GMap.NET.WindowsForms.HelperLineOptions.DontShow;
            this.gMapControl1.LevelsKeepInMemmory = 5;
            this.gMapControl1.Location = new System.Drawing.Point(0, 0);
            this.gMapControl1.MarkersEnabled = true;
            this.gMapControl1.MaxZoom = 2;
            this.gMapControl1.MinZoom = 2;
            this.gMapControl1.MouseWheelZoomType = GMap.NET.MouseWheelZoomType.MousePositionAndCenter;
            this.gMapControl1.Name = "gMapControl1";
            this.gMapControl1.NegativeMode = false;
            this.gMapControl1.PolygonsEnabled = true;
            this.gMapControl1.RetryLoadTile = 0;
            this.gMapControl1.RoutesEnabled = true;
            this.gMapControl1.ScaleMode = GMap.NET.WindowsForms.ScaleModes.Integer;
            this.gMapControl1.SelectedAreaFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(65)))), ((int)(((byte)(105)))), ((int)(((byte)(225)))));
            this.gMapControl1.ShowTileGridLines = false;
            this.gMapControl1.Size = new System.Drawing.Size(683, 448);
            this.gMapControl1.TabIndex = 0;
            this.gMapControl1.Zoom = 0D;
            // 
            // tpInformacionPaq
            // 
            this.tpInformacionPaq.Controls.Add(this.groupBox1);
            this.tpInformacionPaq.Location = new System.Drawing.Point(154, 4);
            this.tpInformacionPaq.Name = "tpInformacionPaq";
            this.tpInformacionPaq.Padding = new System.Windows.Forms.Padding(3);
            this.tpInformacionPaq.Size = new System.Drawing.Size(728, 454);
            this.tpInformacionPaq.TabIndex = 1;
            this.tpInformacionPaq.Text = "INFORMACION DEL PAQUETE";
            this.tpInformacionPaq.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.linkLblFragil);
            this.groupBox1.Controls.Add(this.lbltxHORA);
            this.groupBox1.Controls.Add(this.lbltxESTADO);
            this.groupBox1.Controls.Add(this.labeltxCLiente);
            this.groupBox1.Controls.Add(this.lblEstado);
            this.groupBox1.Controls.Add(this.lblHora);
            this.groupBox1.Controls.Add(this.lblCliente);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(46, 38);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(596, 369);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "PAQUETE  Nº";
            // 
            // linkLblFragil
            // 
            this.linkLblFragil.AutoSize = true;
            this.linkLblFragil.LinkColor = System.Drawing.Color.Red;
            this.linkLblFragil.Location = new System.Drawing.Point(486, 341);
            this.linkLblFragil.Name = "linkLblFragil";
            this.linkLblFragil.Size = new System.Drawing.Size(61, 16);
            this.linkLblFragil.TabIndex = 8;
            this.linkLblFragil.TabStop = true;
            this.linkLblFragil.Text = "FRAGIL";
            // 
            // lbltxHORA
            // 
            this.lbltxHORA.AutoSize = true;
            this.lbltxHORA.Location = new System.Drawing.Point(252, 103);
            this.lbltxHORA.Name = "lbltxHORA";
            this.lbltxHORA.Size = new System.Drawing.Size(43, 16);
            this.lbltxHORA.TabIndex = 7;
            this.lbltxHORA.Text = "-------";
            // 
            // lbltxESTADO
            // 
            this.lbltxESTADO.AutoSize = true;
            this.lbltxESTADO.Location = new System.Drawing.Point(252, 144);
            this.lbltxESTADO.Name = "lbltxESTADO";
            this.lbltxESTADO.Size = new System.Drawing.Size(78, 16);
            this.lbltxESTADO.TabIndex = 5;
            this.lbltxESTADO.Text = "Pendiente";
            // 
            // labeltxCLiente
            // 
            this.labeltxCLiente.AutoSize = true;
            this.labeltxCLiente.Location = new System.Drawing.Point(252, 57);
            this.labeltxCLiente.Name = "labeltxCLiente";
            this.labeltxCLiente.Size = new System.Drawing.Size(43, 16);
            this.labeltxCLiente.TabIndex = 4;
            this.labeltxCLiente.Text = "-------";
            // 
            // lblEstado
            // 
            this.lblEstado.AutoSize = true;
            this.lblEstado.Location = new System.Drawing.Point(43, 144);
            this.lblEstado.Name = "lblEstado";
            this.lblEstado.Size = new System.Drawing.Size(61, 16);
            this.lblEstado.TabIndex = 3;
            this.lblEstado.Text = "Estado:";
            // 
            // lblHora
            // 
            this.lblHora.AutoSize = true;
            this.lblHora.Location = new System.Drawing.Point(43, 103);
            this.lblHora.Name = "lblHora";
            this.lblHora.Size = new System.Drawing.Size(125, 16);
            this.lblHora.TabIndex = 1;
            this.lblHora.Text = "Hora de entrega:";
            // 
            // lblCliente
            // 
            this.lblCliente.AutoSize = true;
            this.lblCliente.Location = new System.Drawing.Point(43, 57);
            this.lblCliente.Name = "lblCliente";
            this.lblCliente.Size = new System.Drawing.Size(60, 16);
            this.lblCliente.TabIndex = 0;
            this.lblCliente.Text = "Cliente:";
            // 
            // tpGenerarRep
            // 
            this.tpGenerarRep.BackColor = System.Drawing.Color.White;
            this.tpGenerarRep.Controls.Add(this.rbNoEntregado);
            this.tpGenerarRep.Controls.Add(this.rbEntregado);
            this.tpGenerarRep.Controls.Add(this.pictureBox2);
            this.tpGenerarRep.Controls.Add(this.pictureBox1);
            this.tpGenerarRep.Controls.Add(this.lblDnii);
            this.tpGenerarRep.Controls.Add(this.lblCLIENTE2);
            this.tpGenerarRep.Controls.Add(this.lblDNI);
            this.tpGenerarRep.Controls.Add(this.lblCiente2);
            this.tpGenerarRep.Controls.Add(this.label1);
            this.tpGenerarRep.Location = new System.Drawing.Point(154, 4);
            this.tpGenerarRep.Name = "tpGenerarRep";
            this.tpGenerarRep.Size = new System.Drawing.Size(728, 454);
            this.tpGenerarRep.TabIndex = 2;
            this.tpGenerarRep.Text = "GENERAR REPORTE";
            // 
            // rbNoEntregado
            // 
            this.rbNoEntregado.AutoSize = true;
            this.rbNoEntregado.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbNoEntregado.Location = new System.Drawing.Point(382, 396);
            this.rbNoEntregado.Name = "rbNoEntregado";
            this.rbNoEntregado.Size = new System.Drawing.Size(103, 17);
            this.rbNoEntregado.TabIndex = 8;
            this.rbNoEntregado.Text = "No Entregado";
            this.rbNoEntregado.UseVisualStyleBackColor = true;
            // 
            // rbEntregado
            // 
            this.rbEntregado.AutoSize = true;
            this.rbEntregado.Checked = true;
            this.rbEntregado.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbEntregado.Location = new System.Drawing.Point(214, 396);
            this.rbEntregado.Name = "rbEntregado";
            this.rbEntregado.Size = new System.Drawing.Size(83, 17);
            this.rbEntregado.TabIndex = 7;
            this.rbEntregado.TabStop = true;
            this.rbEntregado.Text = "Entregado";
            this.rbEntregado.UseVisualStyleBackColor = true;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Location = new System.Drawing.Point(417, 264);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(100, 50);
            this.pictureBox2.TabIndex = 9;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(187, 264);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(100, 50);
            this.pictureBox1.TabIndex = 10;
            this.pictureBox1.TabStop = false;
            // 
            // lblDnii
            // 
            this.lblDnii.AutoSize = true;
            this.lblDnii.Location = new System.Drawing.Point(211, 149);
            this.lblDnii.Name = "lblDnii";
            this.lblDnii.Size = new System.Drawing.Size(19, 13);
            this.lblDnii.TabIndex = 4;
            this.lblDnii.Text = "----";
            // 
            // lblCLIENTE2
            // 
            this.lblCLIENTE2.AutoSize = true;
            this.lblCLIENTE2.Location = new System.Drawing.Point(211, 95);
            this.lblCLIENTE2.Name = "lblCLIENTE2";
            this.lblCLIENTE2.Size = new System.Drawing.Size(22, 13);
            this.lblCLIENTE2.TabIndex = 3;
            this.lblCLIENTE2.Text = "-----";
            // 
            // lblDNI
            // 
            this.lblDNI.AutoSize = true;
            this.lblDNI.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDNI.Location = new System.Drawing.Point(122, 144);
            this.lblDNI.Name = "lblDNI";
            this.lblDNI.Size = new System.Drawing.Size(41, 20);
            this.lblDNI.TabIndex = 2;
            this.lblDNI.Text = "DNI:";
            // 
            // lblCiente2
            // 
            this.lblCiente2.AutoSize = true;
            this.lblCiente2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCiente2.Location = new System.Drawing.Point(122, 90);
            this.lblCiente2.Name = "lblCiente2";
            this.lblCiente2.Size = new System.Drawing.Size(62, 20);
            this.lblCiente2.TabIndex = 1;
            this.lblCiente2.Text = "Cliente:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(219, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(243, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "INFORME DE ENTREGA";
            // 
            // btnSiguiente
            // 
            this.btnSiguiente.Location = new System.Drawing.Point(776, 527);
            this.btnSiguiente.Name = "btnSiguiente";
            this.btnSiguiente.Size = new System.Drawing.Size(136, 41);
            this.btnSiguiente.TabIndex = 4;
            this.btnSiguiente.Text = "Siguiente";
            this.btnSiguiente.UseVisualStyleBackColor = true;
            this.btnSiguiente.Click += new System.EventHandler(this.btnSiguiente_Click);
            // 
            // SOFTWARE_DE_APOYO
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(934, 601);
            this.Controls.Add(this.btnSiguiente);
            this.Controls.Add(this.tabControl1);
            this.MaximumSize = new System.Drawing.Size(950, 640);
            this.MinimumSize = new System.Drawing.Size(950, 640);
            this.Name = "SOFTWARE_DE_APOYO";
            this.Text = "SOFTWARE_DE_APOYO";
            this.Load += new System.EventHandler(this.SOFTWARE_DE_APOYO_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tpInformacionPaq.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tpGenerarRep.ResumeLayout(false);
            this.tpGenerarRep.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tpInformacionPaq;
        private System.Windows.Forms.TabPage tpGenerarRep;
        private GMap.NET.WindowsForms.GMapControl gMapControl1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label labeltxCLiente;
        private System.Windows.Forms.Label lblEstado;
        private System.Windows.Forms.Label lblHora;
        private System.Windows.Forms.Label lblCliente;
        private System.Windows.Forms.Label lbltxHORA;
        private System.Windows.Forms.Label lbltxESTADO;
        private System.Windows.Forms.LinkLabel linkLblFragil;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblDnii;
        private System.Windows.Forms.Label lblCLIENTE2;
        private System.Windows.Forms.Label lblDNI;
        private System.Windows.Forms.Label lblCiente2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton rbNoEntregado;
        private System.Windows.Forms.RadioButton rbEntregado;
        private System.Windows.Forms.Button btnSiguiente;
    }
}