﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GMap.NET.WindowsForms;
using GMap.NET;
using GMap.NET.MapProviders;
using GMap.NET.WindowsForms.Markers;
using GMap.NET.WindowsForms.ToolTips;
using SIDPRO_REPARTIDOR.Repositorios;
using MySql.Data.MySqlClient;
using SIDPRO_REPARTIDOR.Servicios;

namespace SIDPRO_REPARTIDOR.Presentación
{
    public partial class LISTA_DE_PAQUETES : Form
    {
        private SOFTWARE_DE_APOYO ap;
        private string nombreZona;
        private GMapOverlay almacenOverlay;
        private GMapOverlay poligonoOverlay;
        private List<Paquete> misPaquetes;
        private List<GMarkerGoogle> marcadores;
        private Zona miZona;
        private int filaAnterior;
        public LISTA_DE_PAQUETES(string nombreZona)
        {
            this.nombreZona = nombreZona;
            poligonoOverlay = new GMapOverlay("Poligono3");
            almacenOverlay = new GMapOverlay("Marcador3");
            marcadores = new List<GMarkerGoogle>();
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterScreen;
        }

        private void LISTA_DE_PAQUETES_Load(object sender, EventArgs e)
        {

            label2.Text = label2.Text + nombreZona;

            //Iniciar el mapa
            gMapControl1.DragButton = MouseButtons.Left;
            gMapControl1.CanDragMap = true;
            gMapControl1.MapProvider = GMapProviders.GoogleMap;
            gMapControl1.MinZoom = 0;
            gMapControl1.MaxZoom = 24;
            gMapControl1.AutoScroll = true;

            gMapControl1.Overlays.Add(poligonoOverlay);
            gMapControl1.Overlays.Add(almacenOverlay);
            Refrescar();
        }

        private void dataGridView1_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            int fila = e.RowIndex;
            try
            {
                if(fila != filaAnterior)
                {
                    marcadores[fila] = new GMarkerGoogle(new PointLatLng(misPaquetes[fila].Coordenadas.lat, misPaquetes[fila].Coordenadas.lng), GMarkerGoogleType.blue);
                    marcadores[filaAnterior] = new GMarkerGoogle(new PointLatLng(misPaquetes[filaAnterior].Coordenadas.lat, misPaquetes[filaAnterior].Coordenadas.lng), GMarkerGoogleType.green);
                    almacenOverlay.Markers[filaAnterior] = marcadores[filaAnterior];
                    almacenOverlay.Markers[fila] = marcadores[fila];
                    filaAnterior = fila;

                    //actualizar el mapa
                    gMapControl1.Zoom = gMapControl1.Zoom + 1;
                    gMapControl1.Zoom = gMapControl1.Zoom - 1;
                }
            }
            catch (ArgumentOutOfRangeException)
            {
            }
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            int fila = e.RowIndex;
            try
            {
                ap = new SOFTWARE_DE_APOYO();
                ap.MiPaquete = misPaquetes[fila];
                ap.Show();
            }
            catch (ArgumentOutOfRangeException)
            {
            }
        }

        private void btnRefrescar_Click(object sender, EventArgs e)
        {
            Refrescar();
        }

        private void Refrescar()
        {

            miZona = Sistema.obtenerZona(nombreZona);

            gMapControl1.Position = new PointLatLng(miZona.LatMed, miZona.LngMed);
            gMapControl1.Zoom = 12;

            GMapPolygon poligono = miZona.Poligono;
            poligonoOverlay.Polygons.Clear();
            poligonoOverlay.Polygons.Add(poligono);

            misPaquetes = Sistema.obtenerPaquetesDeZona(miZona);

            marcadores.Clear();

            almacenOverlay.Markers.Clear();

            dataGridView1.Rows.Clear();

            filaAnterior = 0;

            for (int i = 0; i < misPaquetes.Count; i++)
            {
                marcadores.Add(new GMarkerGoogle(new PointLatLng(misPaquetes[i].Coordenadas.lat, misPaquetes[i].Coordenadas.lng), GMarkerGoogleType.green));
                almacenOverlay.Markers.Add(marcadores[i]);
                string informacion = "Cliente: " + misPaquetes[i].Nombres + " " + misPaquetes[i].Apellidos + " Teléfono: " + misPaquetes[i].Telefono;
                dataGridView1.Rows.Add(misPaquetes[i].Codigo, informacion);
            }
            marcadores[0] = new GMarkerGoogle(new PointLatLng(misPaquetes[0].Coordenadas.lat, misPaquetes[0].Coordenadas.lng), GMarkerGoogleType.blue);
            almacenOverlay.Markers[0] = marcadores[0];

            almacenOverlay.Markers.Add(new GMarkerGoogle(new PointLatLng(Sistema.LatAlmacen, Sistema.LngAlmacen), GMarkerGoogleType.red));

            //actualizar el mapa
            gMapControl1.Zoom = gMapControl1.Zoom + 1;
            gMapControl1.Zoom = gMapControl1.Zoom - 1;
        }

        private void LISTA_DE_PAQUETES_Enter(object sender, EventArgs e)
        {
            Refrescar();
        }
    }
}
