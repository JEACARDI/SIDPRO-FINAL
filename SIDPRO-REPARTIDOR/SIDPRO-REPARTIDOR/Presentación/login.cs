﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using SIDPRO_REPARTIDOR.Servicios;

namespace SIDPRO_REPARTIDOR.Presentación
{
    public partial class login : Form
    {
        private MySqlConnection miConexion;

        public login()
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterScreen;
        }
        private void login_Load(object sender, EventArgs e)
        {
            miConexion = ConexionBD.Instancia;
        }

        private void btnIngresar_Click(object sender, EventArgs e)
        {
            verificarIngreso();
        }

        private void FocusLeaveUsu(object sender, EventArgs e)
        {
            if(txtUsuario.Text == "")
                label1.Visible = true;
        }

        private void FocusLeavePass(object sender, EventArgs e)
        {
            if(txtPassword.Text == "")
                label2.Visible = true;
        }

        private void TextChangedUsu(object sender, EventArgs e)
        {
            if(txtUsuario.Text != "")
            {
                label1.Visible = false;
            }
            else
            {
                label1.Visible = true;
            }
        }

        private void TextChangedPass(object sender, EventArgs e)
        {
            if (txtPassword.Text != "")
            {
                label2.Visible = false;
            }
            else
            {
                label2.Visible = true;
            }
        }

        private void verificarIngreso()
        {
            string usu = txtUsuario.Text;
            string pas = txtPassword.Text;

            if (usu.Length == 0 || pas.Length == 0)
            {
                MessageBox.Show("Espacios en blanco!");
            }
            else
            {
                miConexion.Open();
                MySqlCommand cmd = miConexion.CreateCommand();
                cmd.CommandText = "SELECT Contraseña FROM sidpro.repartidor WHERE idRepartidor = '" + usu + "';";
                string aux = null;
                MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    aux = reader.GetString("Contraseña");
                }
                if (pas == aux)
                {
                    SelectorZonas sz = new SelectorZonas(usu);
                    sz.Show();
                    this.Visible = false;
                }
                else if (aux == null)
                {
                    MessageBox.Show("Usuario no encontrado!");
                }
                else
                {
                    MessageBox.Show("Contraseña Incorrecta!");
                }
                miConexion.Close();
            }
        }

        private void txtUsuario_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar == '\r')
                verificarIngreso();
        }

        private void txtPassword_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
                verificarIngreso();
        }
    }
}
