﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GMap.NET.WindowsForms;
using GMap.NET;
using GMap.NET.MapProviders;
using GMap.NET.WindowsForms.Markers;
using GMap.NET.WindowsForms.ToolTips;
using SIDPRO_REPARTIDOR.Repositorios;
using MySql.Data.MySqlClient;
using SIDPRO_REPARTIDOR.Servicios;

namespace SIDPRO_REPARTIDOR.Presentación
{
    public partial class SOFTWARE_DE_APOYO : Form
    {
        private Paquete miPaquete;
        private GMapOverlay markerOverlay;
        private GMapOverlay routeOverlay;

        internal Paquete MiPaquete
        {
            get
            {
                return miPaquete;
            }

            set
            {
                miPaquete = value;
            }
        }

        public SOFTWARE_DE_APOYO()
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterScreen;
        }

        private void tabControl1_DrawItem(object sender, DrawItemEventArgs e)
        {
            Graphics g = e.Graphics;
            Brush _textBrush;

            // Get the item from the collection.
            TabPage _tabPage = tabControl1.TabPages[e.Index];

            // Get the real bounds for the tab rectangle.
            Rectangle _tabBounds = tabControl1.GetTabRect(e.Index);

            if (e.State == DrawItemState.Selected)
            {

                // Draw a different background color, and don't paint a focus rectangle.
                _textBrush = new SolidBrush(Color.Black);
                g.FillRectangle(Brushes.Gray, e.Bounds);
            }
            else
            {
                _textBrush = new System.Drawing.SolidBrush(e.ForeColor);
                g.FillRectangle(Brushes.LightGray, e.Bounds);
            }

            // Use our own font.
            Font _tabFont = new Font("Arial", (float)10.0, FontStyle.Bold, GraphicsUnit.Pixel);

            // Draw string. Center the text.
            StringFormat _stringFlags = new StringFormat();
            _stringFlags.Alignment = StringAlignment.Center;
            _stringFlags.LineAlignment = StringAlignment.Center;
            g.DrawString(_tabPage.Text, _tabFont, _textBrush, _tabBounds, new StringFormat(_stringFlags));
        }

        private void SOFTWARE_DE_APOYO_Load(object sender, EventArgs e)
        {
            //Iniciar el mapa
            gMapControl1.DragButton = MouseButtons.Left;
            gMapControl1.CanDragMap = true;
            gMapControl1.MapProvider = GMapProviders.GoogleMap;
            gMapControl1.Position = new PointLatLng(miPaquete.Coordenadas.lat, miPaquete.Coordenadas.lng);
            gMapControl1.MinZoom = 0;
            gMapControl1.MaxZoom = 24;
            gMapControl1.Zoom = 12;
            gMapControl1.AutoScroll = true;

            markerOverlay = new GMapOverlay("Marcador4");
            markerOverlay.Markers.Add(new GMarkerGoogle(new PointLatLng(Sistema.LatAlmacen, Sistema.LngAlmacen), GMarkerGoogleType.red));
           markerOverlay.Markers.Add(new GMarkerGoogle(new PointLatLng(miPaquete.Coordenadas.lat, miPaquete.Coordenadas.lng), GMarkerGoogleType.blue));


            PointLatLng inicio = new PointLatLng(Sistema.LatAlmacen, Sistema.LngAlmacen);
            PointLatLng fin = new PointLatLng(miPaquete.Coordenadas.lat, miPaquete.Coordenadas.lng);
            MapRoute ruta = GMap.NET.MapProviders.OpenStreetMapProvider.Instance.GetRoute(inicio, fin, false, false, 20);
            GMapRoute r = new GMapRoute(ruta.Points, "Mi ruta");
            routeOverlay = new GMapOverlay("Rutas1");
            routeOverlay.Routes.Add(r);

            gMapControl1.Overlays.Add(routeOverlay);
            gMapControl1.Overlays.Add(markerOverlay);

            //actualizar el mapa
            gMapControl1.Zoom = gMapControl1.Zoom + 1;
            gMapControl1.Zoom = gMapControl1.Zoom - 1;

            lblCLIENTE2.Text = miPaquete.Nombres + " " + miPaquete.Apellidos;
            lblDnii.Text = Convert.ToString(miPaquete.DNI1);
            labeltxCLiente.Text = miPaquete.Nombres + " " + miPaquete.Apellidos;
            string horario;

            if(miPaquete.Horario == 1)
            {
                horario = "08:00am - 12:00pm";
            }else if(miPaquete.Horario == 2)
            {
                horario = "12:00pm - 04:00pm";
            }else if(miPaquete.Horario == 3)
            {
                horario = "04:00pm - 08:00pm";
            }else
            {
                horario = "Desconocido";
            }
            lbltxHORA.Text = horario;

            string fragil;
            if (miPaquete.Fragil)
            {
                fragil = "FRAGIL";
            }else
            {
                fragil = "NO FRAGIL";
            }

            linkLblFragil.Text = fragil;
        }

        private void btnSiguiente_Click(object sender, EventArgs e)
        {
            if (rbEntregado.Checked)
            {
                Sistema.mandarReporteEntregado(miPaquete);
            }else
            {
                Sistema.mandarReporteNoEntregado(miPaquete);
            }
            MessageBox.Show("El reporte fue enviado con éxito!");
            this.Dispose();
        }
    }
}
