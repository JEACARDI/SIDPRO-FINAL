﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIDPRO_REPARTIDOR.Repositorios
{
    struct coord
    {
        public double lat;
        public double lng;
        public coord(double lat, double lng)
        {
            this.lat = lat; this.lng = lng;
        }
    }
    struct medidas
    {
        public float ancho;
        public float largo;
        public float alto;
        public medidas(float ancho, float largo, float alto)
        {
            this.ancho = ancho; this.largo = largo; this.alto = alto;
        }
    }
    class Paquete
    {
        private string codigo;
        private string nombres;
        private string apellidos;
        private string telefono;
        private string correo;
        private coord coordenadas;
        private bool fragil;
        private medidas medida;
        private string idRepartidor;
        private short horario;
        private int DNI;

        public Paquete(string codigo, string nombres, string apellidos, string telefono, string correo, coord coordenadas, bool fragil, medidas medida, short horario, int DNI)
        {
            this.codigo = codigo;
            this.nombres = nombres;
            this.apellidos = apellidos;
            this.telefono = telefono;
            this.correo = correo;
            this.fragil = fragil;
            this.horario = horario;
            this.coordenadas = coordenadas;
            this.medida = medida;
            this.DNI = DNI;
        }

        public string Codigo
        {
            get
            {
                return codigo;
            }

            set
            {
                codigo = value;
            }
        }

        public string Nombres
        {
            get
            {
                return nombres;
            }

            set
            {
                nombres = value;
            }
        }

        public string Apellidos
        {
            get
            {
                return apellidos;
            }

            set
            {
                apellidos = value;
            }
        }

        public string Telefono
        {
            get
            {
                return telefono;
            }

            set
            {
                telefono = value;
            }
        }

        public string Correo
        {
            get
            {
                return correo;
            }

            set
            {
                correo = value;
            }
        }

        public coord Coordenadas
        {
            get
            {
                return coordenadas;
            }

            set
            {
                coordenadas = value;
            }
        }

        public bool Fragil
        {
            get
            {
                return fragil;
            }

            set
            {
                fragil = value;
            }
        }

        public medidas Medida
        {
            get
            {
                return medida;
            }

            set
            {
                medida = value;
            }
        }

        public string IdRepartidor
        {
            get
            {
                return idRepartidor;
            }

            set
            {
                idRepartidor = value;
            }
        }

        public int DNI1
        {
            get
            {
                return DNI;
            }

            set
            {
                DNI = value;
            }
        }

        public short Horario
        {
            get
            {
                return horario;
            }

            set
            {
                horario = value;
            }
        }
    }

}
