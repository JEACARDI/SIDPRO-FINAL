﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using SIDPRO_REPARTIDOR.Servicios;
using System.Windows.Forms;

namespace SIDPRO_REPARTIDOR.Repositorios
{
    class Sistema
    {
        private static double latAlmacen = -12.053329, lngAlmacen = -77.085559;

        private Sistema()
        {
        }

        public static List<Zona> obtenerZonas()
        {
            MySqlConnection miCon = ConexionBD.Instancia;
            miCon.Open();
            MySqlCommand cmd = miCon.CreateCommand();
            cmd.CommandText = "SELECT * FROM sidpro.zona;";
            MySqlDataReader reader = cmd.ExecuteReader();

            List<Zona> zonas = new List<Zona>();

            while (reader.Read())
            {
                string nombre = reader.GetString("codigoZona");
                short vehiculo = reader.GetInt16("vehiculo");
                double latMax = reader.GetDouble("latMax");
                double latMin = reader.GetDouble("latMin");
                double lngMax = reader.GetDouble("lngMax");
                double lngMin = reader.GetDouble("lngMin");
                int cantidadPaquetes = reader.GetInt32("cantidadPaquetes");
                string codRep = null;
                try
                {
                    codRep = reader.GetString("codigoRepartidor");
                }
                catch (System.Data.SqlTypes.SqlNullValueException)
                {
                }
                zonas.Add(new Zona(nombre, latMax, latMin, lngMax, lngMin, vehiculo, cantidadPaquetes,codRep));
            }
            miCon.Close();
            return zonas;
        }

        public static List<Zona> obtenerMisZonas(string idRepartidor)
        {
            List<Zona> aux = new List<Zona>();
            MySqlConnection miCon = ConexionBD.Instancia;
            miCon.Open();
            MySqlCommand cmd = miCon.CreateCommand();
            cmd.CommandText = "SELECT * FROM sidpro.zona WHERE codigoRepartidor = '"+idRepartidor+"';";
            MySqlDataReader reader = cmd.ExecuteReader();


            while (reader.Read())
            {
                string nombre = reader.GetString("codigoZona");
                if (nombre == null)
                    break;
                short vehiculo = reader.GetInt16("vehiculo");
                double latMax = reader.GetDouble("latMax");
                double latMin = reader.GetDouble("latMin");
                double lngMax = reader.GetDouble("lngMax");
                double lngMin = reader.GetDouble("lngMin");
                int cantidadPaquetes = reader.GetInt32("cantidadPaquetes");
                string codRep = reader.GetString("codigoRepartidor");
                aux.Add(new Zona(nombre, latMax, latMin, lngMax, lngMin, vehiculo, cantidadPaquetes, codRep));
            }
            miCon.Close();
            if (aux.Count == 0)
                return null;
            else
                return aux;
        }

        public static List<Paquete> obtenerPaquetesDeZona(Zona miZona)
        {
            List<Paquete> aux = new List<Paquete>();

            MySqlConnection miCon = ConexionBD.Instancia;
            miCon.Open();
            MySqlCommand cmd = miCon.CreateCommand();
            cmd.CommandText = "SELECT * FROM sidpro.paquete WHERE codigoZona = '"+miZona.Nombre+"';";
            MySqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                medidas medida = new medidas(reader.GetFloat("Ancho"), reader.GetFloat("Largo"), reader.GetFloat("Alto"));
                coord coordenada = new coord(reader.GetDouble("Lat"), reader.GetDouble("Long"));
                aux.Add(new Paquete(reader.GetString(0), reader.GetString(1), reader.GetString(2), reader.GetString(3), reader.GetString(4), coordenada, reader.GetBoolean(7), medida, reader.GetInt16("Horario"), reader.GetInt32("DNI")));
            }
            miCon.Close();

            return aux;
        }

        public static Zona obtenerZona(string nombreZona)
        {
            Zona aux = null;

            MySqlConnection miCon = ConexionBD.Instancia;
            miCon.Open();
            MySqlCommand cmd = miCon.CreateCommand();
            cmd.CommandText = "SELECT * FROM sidpro.zona WHERE codigoZona = '" + nombreZona + "';";
            MySqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                aux = new Zona(nombreZona, reader.GetDouble("latMax"), reader.GetDouble("latMin"), reader.GetDouble("lngMax"), reader.GetDouble("lngMin"), reader.GetInt16("vehiculo"), reader.GetInt32("cantidadPaquetes"), reader.GetString("codigoRepartidor"));
            }
            miCon.Close();

            return aux;
        }

        private static string obtenerZonaDePaquete(Paquete p)
        {
            string aux = null;

            MySqlConnection miCon = ConexionBD.Instancia;
            miCon.Open();
            MySqlCommand cmd = miCon.CreateCommand();
            cmd.CommandText = "SELECT codigoZona FROM sidpro.paquete WHERE codigo = '"+p.Codigo+"';";
            MySqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                aux = reader.GetString("codigoZona");
            }

            miCon.Close();

            return aux;
        }

        public static void mandarReporteEntregado(Paquete p)
        {
            string borrarPaquete = "DELETE FROM `sidpro`.`paquete` WHERE `codigo`= '"+p.Codigo+"';";
            string fragil;
            if (p.Fragil)
            {
                fragil = "1";
            }else
            {
                fragil = "0";
            }
            string agregarPaquete = "\nINSERT INTO `sidpro`.`paquete_entregado` (`codigo`, `Nombres`, `Apellidos`, `Teléfono`, `Correo`, `Lat`, `Long`, `Fragil`, `Largo`, `Ancho`, `Alto`, `Horario`, `DNI`) VALUES ('" + p.Codigo + "', '" + p.Nombres + "', '" + p.Apellidos + "', '" + p.Telefono + "', '" + p.Correo + "', '" + p.Coordenadas.lat + "', '" + p.Coordenadas.lng + "', '" + fragil + "', '" + p.Medida.largo + "', '" + p.Medida.ancho + "', '" + p.Medida.alto + "', '" + p.Horario + "', '" + p.DNI1 + "');";
            string codigoReporte = generarCodigoEntregado();
            string agregarReporte = "\nINSERT INTO `sidpro`.`reportes_entregado` (`codigoReporte`, `Informacion`) VALUES ('"+codigoReporte+ "', 'El paquete "+p.Codigo+" entregado con éxito');";
            string nombreZona = obtenerZonaDePaquete(p);
            int cantidad = cantidadDePaquetes(nombreZona);
            cantidad--;
            string quitarPaquete = "\nUPDATE `sidpro`.`zona` SET `cantidadPaquetes`= '" + cantidad + "' WHERE `codigoZona`= '" + nombreZona + "';";

            //Se actualiza la bD
            MySqlConnection miCon = ConexionBD.Instancia;
             miCon.Open(); // Se abre la conexión
                           // Se crea un comando para ejecutar en MySQL
             MySqlCommand cmd = miCon.CreateCommand();
             cmd.CommandText = borrarPaquete+agregarPaquete+agregarReporte+quitarPaquete;
             // Se ejecuta el comando
             cmd.ExecuteNonQuery();
             // Se cierra la conexión y la ventana
             miCon.Close();
        }

        private static string generarCodigoEntregado()
        {
            string aux = null;

            MySqlConnection miCon = ConexionBD.Instancia;
            miCon.Open();
            MySqlCommand cmd = miCon.CreateCommand();
            cmd.CommandText = "SELECT codigoReporte FROM sidpro.reportes_entregado;";
            MySqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                aux = reader.GetString("codigoReporte");
            }
            if (aux == null)
            {
                aux = "1";
            }else
            {
                int a = Convert.ToInt32(aux);
                a++;
                aux = a.ToString();
            }
            miCon.Close();

            return aux;
        }

        public static void mandarReporteNoEntregado(Paquete p)
        {
            string reiniciarPaquete = "UPDATE `sidpro`.`paquete` SET `codigoZona`=NULL, `Horario`=NULL WHERE `codigo`='"+p.Codigo+"';";
            string codigoReporte = generarCodigoNoEntregado();
            string agregarReporte = "\nINSERT INTO `sidpro`.`reportes_no_entregado` (`codigoReporte`, `Informacion`) VALUES ('" + codigoReporte + "', 'El paquete "+p.Codigo+" no fue entregado');";
            string nombreZona = obtenerZonaDePaquete(p);
            int cantidad = cantidadDePaquetes(nombreZona);
            cantidad--;
            string quitarPaquete = "\nUPDATE `sidpro`.`zona` SET `cantidadPaquetes`= '" + cantidad + "' WHERE `codigoZona`= '" + nombreZona + "';";

            //Se actualiza la bD
            MySqlConnection miCon = ConexionBD.Instancia;
             miCon.Open(); // Se abre la conexión
                           // Se crea un comando para ejecutar en MySQL
             MySqlCommand cmd = miCon.CreateCommand();
             cmd.CommandText = reiniciarPaquete + agregarReporte+quitarPaquete;
             // Se ejecuta el comando
             cmd.ExecuteNonQuery();
             // Se cierra la conexión y la ventana
             miCon.Close();
        }

        private static string generarCodigoNoEntregado()
        {
            string aux = null;

            MySqlConnection miCon = ConexionBD.Instancia;
            miCon.Open();
            MySqlCommand cmd = miCon.CreateCommand();
            cmd.CommandText = "SELECT codigoReporte FROM sidpro.reportes_no_entregado;";
            MySqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                aux = reader.GetString("codigoReporte");
            }
            if (aux == null)
            {
                aux = "1";
            }
            else
            {
                int a = Convert.ToInt32(aux);
                a++;
                aux = a.ToString();
            }
            miCon.Close();

            return aux;
        }

        private static int cantidadDePaquetes(string nombreZona)
        {
            int aux = 0;

            MySqlConnection miCon = ConexionBD.Instancia;
            miCon.Open();
            MySqlCommand cmd = miCon.CreateCommand();
            cmd.CommandText = "SELECT cantidadPaquetes FROM sidpro.zona WHERE codigoZona = '"+nombreZona+"';";
            MySqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                aux = reader.GetInt32("cantidadPaquetes");
            }
            miCon.Close();

            return aux;
        }

        public static double LatAlmacen
        {
            get
            {
                return latAlmacen;
            }

            set
            {
                latAlmacen = value;
            }
        }

        public static double LngAlmacen
        {
            get
            {
                return lngAlmacen;
            }

            set
            {
                lngAlmacen = value;
            }
        }
    }
}
