﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GMap.NET.WindowsForms;
using GMap.NET;

namespace SIDPRO_REPARTIDOR.Repositorios
{
    class Zona
    {
        private double separacion = 0.001;
        private double latMax, latMin, lngMax, lngMin;
        private double latMed=0, lngMed=0;
        private GMapPolygon poligono=null;
        private string nombre;
        private short vehiculo;
        private string idRepartidor=null;
        private int cantidadPaquetes;

        public Zona(string nombre, double latMax, double latMin, double lngMax, double lngMin, short vehiculo, int cantidadPaquetes, string idRepartidor)
        {
            this.cantidadPaquetes = cantidadPaquetes;  this.vehiculo = vehiculo;  this.nombre = nombre; this.latMax = latMax; this.latMin = latMin; this.lngMax = lngMax; this.lngMin = lngMin; this.idRepartidor = idRepartidor;
        }

        public double LatMax
        {
            get
            {
                return latMax;
            }

            set
            {
                latMax = value;
            }
        }

        public double LatMin
        {
            get
            {
                return latMin;
            }

            set
            {
                latMin = value;
            }
        }

        public double LngMax
        {
            get
            {
                return lngMax;
            }

            set
            {
                lngMax = value;
            }
        }

        public double LngMin
        {
            get
            {
                return lngMin;
            }

            set
            {
                lngMin = value;
            }
        }

        public string Nombre
        {
            get
            {
                return nombre;
            }

            set
            {
                nombre = value;
            }
        }

        public GMapPolygon Poligono
        {
            get
            {
                //Se genera un cuadrante teniendo en cuenta las coordenadas máximas y mínimas
                List<PointLatLng> puntos = new List<PointLatLng>();
                puntos.Add(new PointLatLng(latMin - separacion, lngMin - separacion));
                puntos.Add(new PointLatLng(latMin - separacion, lngMax + separacion));
                puntos.Add(new PointLatLng(latMax + separacion, lngMax + separacion));
                puntos.Add(new PointLatLng(latMax + separacion, lngMin - separacion));
                poligono = new GMapPolygon(puntos, nombre);
                return poligono;
            }

            set
            {
                poligono = value;
            }
        }

        public short Vehiculo
        {
            get
            {
                return vehiculo;
            }

            set
            {
                vehiculo = value;
            }
        }

        public double LatMed
        {
            get
            {
                if (latMed == 0)
                    latMed = (latMax + latMin) / 2;
                return latMed;
            }

            set
            {
                latMed = value;
            }
        }

        public double LngMed
        {
            get
            {
                if (lngMed == 0)
                    lngMed = (lngMax + lngMin) / 2;
                return lngMed;
            }

            set
            {
                lngMed = value;
            }
        }

        public string IdRepartidor
        {
            get
            {
                return idRepartidor;
            }

            set
            {
                idRepartidor = value;
            }
        }

        public int CantidadPaquetes
        {
            get
            {
                return cantidadPaquetes;
            }

            set
            {
                cantidadPaquetes = value;
            }
        }
    }
}
