-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: localhost    Database: sidpro
-- ------------------------------------------------------
-- Server version	5.7.12-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `administrador`
--

DROP TABLE IF EXISTS `administrador`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `administrador` (
  `codigoAdministrador` varchar(10) NOT NULL,
  `contraseña` varchar(45) NOT NULL,
  PRIMARY KEY (`codigoAdministrador`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `administrador`
--

LOCK TABLES `administrador` WRITE;
/*!40000 ALTER TABLE `administrador` DISABLE KEYS */;
INSERT INTO `administrador` VALUES ('adm0001','12345');
/*!40000 ALTER TABLE `administrador` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `paquete`
--

DROP TABLE IF EXISTS `paquete`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paquete` (
  `codigo` varchar(10) NOT NULL,
  `Nombres` varchar(30) NOT NULL,
  `Apellidos` varchar(30) NOT NULL,
  `Teléfono` varchar(12) NOT NULL,
  `Correo` varchar(45) NOT NULL,
  `Lat` double(15,12) NOT NULL,
  `Long` double(15,12) NOT NULL,
  `Fragil` tinyint(1) NOT NULL,
  `Largo` float(5,2) NOT NULL,
  `Ancho` float(5,2) NOT NULL,
  `Alto` float(5,2) NOT NULL,
  `codigoZona` varchar(10) DEFAULT NULL,
  `Horario` tinyint(4) DEFAULT NULL,
  `DNI` int(11) NOT NULL,
  PRIMARY KEY (`codigo`),
  UNIQUE KEY `codigo_UNIQUE` (`codigo`),
  KEY `fk_paquete_zona1_idx` (`codigoZona`),
  CONSTRAINT `fk_paquete_zona1` FOREIGN KEY (`codigoZona`) REFERENCES `zona` (`codigoZona`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `paquete`
--

LOCK TABLES `paquete` WRITE;
/*!40000 ALTER TABLE `paquete` DISABLE KEYS */;
INSERT INTO `paquete` VALUES ('CL0001','Liliana Belén','Lopez Meneses','978813125','liliana@hotmail.com',-12.049816000000,-77.091781000000,1,30.00,30.00,30.00,NULL,1,77155721),('CL0002','Rosa Isabel','Torres Ramirez','56214447','rosatorres85@hotmail.com',-12.052226460000,-77.093102930000,1,20.00,25.00,20.00,NULL,1,54876543),('CL0003','Josè Antonio','Perez Lopez','74237651','joseperez@hotmail.com',-12.051554940000,-77.088489530000,1,10.00,30.00,40.00,NULL,3,58753153),('CL0004','Luis Daniel','Correa Mejía','54224698','luiscorrea41@hotmail.com',-12.052583190000,-77.036647800000,1,25.50,20.00,30.00,NULL,2,45488753),('CL0005','Laura Lisset','Leiva Alba','44280585','lauraleiva@hotmail.com',-12.052457290000,-77.091407780000,0,30.00,40.00,20.00,NULL,1,24587453),('CL0006','Manuel','Pardavé Arce','73378165','manuelpardave@hotmail.com',-12.049393510000,-77.033472060000,1,10.00,60.00,40.00,NULL,3,23154856),('CL0007','Jose Eduardo','Arteaga Campos','56231494','hosearteaga@hotmail.com',-12.101431050000,-76.976051330000,0,40.00,30.00,30.00,NULL,2,21554856),('CL0008','Edgar Paul','Flores Guerra','996644910','edgarflores@hotmail.com',-12.078267200000,-77.064285280000,0,50.00,10.00,10.00,NULL,2,21566324),('CL0009','Jean Piero','Huayta Nuñez','954154416','jeanpierohuayta@hotmail.com',-12.081624400000,-77.027549740000,0,20.00,20.00,20.00,NULL,2,88745125),('CL0010','Anthony','Grados Matos','5366268','anthonygrados@hotmail.com',-12.090017230000,-77.065658570000,1,30.00,50.00,20.00,NULL,2,23488788),('CL0011','Julio Marcelo','Ampudia Mejia','955487595','julioampudia@hotmail.com',-12.050736540000,-77.095527650000,0,20.00,40.00,10.00,NULL,1,66565986),('CL0012','Sandra Elsa','Reyes Pardo','988995980','sandrareyes@hotmail.com',-12.056108600000,-77.126255040000,1,70.00,30.00,30.00,NULL,3,66666666),('CL0013','Carla Paola','Salazar Contreras','4859632','carlasalazar@hotmail.com',-12.071552660000,-77.102222440000,1,30.00,20.00,50.00,NULL,2,65659884),('CL0014','Melissa Camila','Chacon Leon','988739542','melissachacon@hotmail.com',-12.099165110000,-77.042913440000,0,50.00,10.00,40.00,NULL,1,61648412),('CL0015','Noelia Cristel','Maguiña Maguiña','999612145','noelia13@gmail.com',-12.120564890000,-77.031326290000,1,40.00,60.00,30.00,NULL,3,32325154),('CL0016','Isaac','Jejeje Mimimi','4534867','tugatitadivina@hotmail.com',-12.057955220000,-77.141447070000,0,30.00,30.00,10.00,NULL,1,15154855),('CL0017','Sandro Paul','Ñuflo Xafli','4758645','sandro_ñuflo@gmail.com',-12.067607800000,-77.111320500000,0,10.00,20.00,40.00,NULL,2,22123154),('CL0018','Nancy Fabiola','Yañez Vargas','978562466','Nancy.yz@gmail.com',-12.014472340000,-77.081365590000,1,30.00,40.00,60.00,NULL,3,31548566),('CL0019','Paula Lesly','Perez Aguilar','4785920','paula.ag@hotmail.com',-11.990377440000,-77.098960880000,1,60.00,30.00,20.00,NULL,1,32314874),('CL0020','Salvador','Rodriguez Peña','5234886','Salvador.rod@gmail.com',-11.983996550000,-77.073469160000,1,40.00,50.00,40.00,NULL,3,11124127),('CL0021','Josesito','Perez Rodriguez','4887963','jox.rod@hotmail.com',-12.181985302295,-77.006607055664,0,20.00,10.00,20.00,NULL,1,77547575);
/*!40000 ALTER TABLE `paquete` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `paquete_entregado`
--

DROP TABLE IF EXISTS `paquete_entregado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paquete_entregado` (
  `codigo` varchar(10) NOT NULL,
  `Nombres` varchar(30) NOT NULL,
  `Apellidos` varchar(30) NOT NULL,
  `Teléfono` varchar(12) NOT NULL,
  `Correo` varchar(45) NOT NULL,
  `Lat` double(15,12) NOT NULL,
  `Long` double(15,12) NOT NULL,
  `Fragil` tinyint(1) NOT NULL,
  `Largo` float(5,2) NOT NULL,
  `Ancho` float(5,2) NOT NULL,
  `Alto` float(5,2) NOT NULL,
  `Horario` tinyint(4) DEFAULT NULL,
  `DNI` int(11) NOT NULL,
  PRIMARY KEY (`codigo`),
  UNIQUE KEY `codigo_UNIQUE` (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `paquete_entregado`
--

LOCK TABLES `paquete_entregado` WRITE;
/*!40000 ALTER TABLE `paquete_entregado` DISABLE KEYS */;
/*!40000 ALTER TABLE `paquete_entregado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `repartidor`
--

DROP TABLE IF EXISTS `repartidor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repartidor` (
  `idRepartidor` varchar(10) NOT NULL,
  `Nombres` varchar(30) NOT NULL,
  `Apellidos` varchar(30) NOT NULL,
  `Contraseña` varchar(30) NOT NULL,
  PRIMARY KEY (`idRepartidor`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `repartidor`
--

LOCK TABLES `repartidor` WRITE;
/*!40000 ALTER TABLE `repartidor` DISABLE KEYS */;
INSERT INTO `repartidor` VALUES ('rep0001','Carlos Enrique','La Rosa Sánchez Arellán','12345');
/*!40000 ALTER TABLE `repartidor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reportes_entregado`
--

DROP TABLE IF EXISTS `reportes_entregado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reportes_entregado` (
  `codigoReporte` varchar(10) NOT NULL,
  `Informacion` varchar(300) NOT NULL,
  PRIMARY KEY (`codigoReporte`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reportes_entregado`
--

LOCK TABLES `reportes_entregado` WRITE;
/*!40000 ALTER TABLE `reportes_entregado` DISABLE KEYS */;
/*!40000 ALTER TABLE `reportes_entregado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reportes_no_entregado`
--

DROP TABLE IF EXISTS `reportes_no_entregado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reportes_no_entregado` (
  `codigoReporte` varchar(10) NOT NULL,
  `Informacion` varchar(300) NOT NULL,
  PRIMARY KEY (`codigoReporte`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reportes_no_entregado`
--

LOCK TABLES `reportes_no_entregado` WRITE;
/*!40000 ALTER TABLE `reportes_no_entregado` DISABLE KEYS */;
/*!40000 ALTER TABLE `reportes_no_entregado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zona`
--

DROP TABLE IF EXISTS `zona`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zona` (
  `codigoZona` varchar(10) NOT NULL,
  `codigoRepartidor` varchar(10) DEFAULT NULL,
  `cantidadPaquetes` tinyint(4) DEFAULT NULL,
  `vehiculo` tinyint(4) DEFAULT NULL,
  `latMax` double(15,12) DEFAULT NULL,
  `latMin` double(15,12) DEFAULT NULL,
  `lngMax` double(15,12) DEFAULT NULL,
  `lngMin` double(15,12) DEFAULT NULL,
  PRIMARY KEY (`codigoZona`),
  KEY `fk_zona_repartidor_idx` (`codigoRepartidor`),
  CONSTRAINT `fk_zona_repartidor` FOREIGN KEY (`codigoRepartidor`) REFERENCES `repartidor` (`idRepartidor`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zona`
--

LOCK TABLES `zona` WRITE;
/*!40000 ALTER TABLE `zona` DISABLE KEYS */;
/*!40000 ALTER TABLE `zona` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-11-22 17:22:08
